module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    "plugin:vue/vue3-strongly-recommended",
    "@vue/standard",
    "@vue/typescript/recommended"

  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    camelcase: 0,
    eqeqeq: 1,
    indent: 1,
    quotes: [1, "double", "avoid-escape"],
    semi: [1, "always", { omitLastInOneLineBlock: true }],
    "quote-props": 1,
    "comma-dangle": [1, { arrays: "never", objects: "never", imports: "never", exports: "never", functions: "never" }],
    "brace-style": 1,
    "comma-spacing": 1,
    "no-console": 1,
    "prefer-const": 1,
    "space-before-function-paren": 0,
    "no-empty-pattern": 0,
    "no-useless-return": 0,
    "vue/no-multiple-template-root": 0,
    "vue/html-indent": [1, 2, { attribute: 1, baseIndent: 1, closeBracket: 0, alignAttributesVertically: false, ignores: [] }],
    "vue/no-unused-components": 1,
    "vue/no-use-v-if-with-v-for": [1, { allowUsingIterationVar: true }],
    "vue/html-self-closing": 0,
    "vue/require-explicit-emits": [0, { allowProps: true }]
  }
};
