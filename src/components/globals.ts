import { createApp } from "vue";
import App from "../App.vue";

import Loader from "./Loader.vue";
import SvgInjector from "./SvgInjector.vue";

const app = createApp(App);

app.component("Loader", Loader);
app.component("SvgInjector", SvgInjector);

export default app;
