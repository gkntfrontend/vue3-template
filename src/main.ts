import App from "./components/globals";
import router from "./router";
import store from "./store";

import "./assets/styles/main.scss";

App.use(store).use(router).mount("#app");
