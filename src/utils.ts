// UUID Timestamp Generator
// npm i uuid -D
import { v4 as uuidv4 } from "uuid";

// SVG Injector
// npm i svg-injector -D
import svgInjector from "svg-injector";

/**
 ** Functions Start
 */
export function randomId (): string {
  return uuidv4();
}

export function injectSvg (): void {
  svgInjector(document.querySelectorAll("img.sevege"));
}

export function awaiter(ms: number): Promise<unknown> {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export function seperateThousand (num: number): string {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export function capitalize (text: string): string {
  const wordsArray: string[] = text.toLowerCase().split(" ");
  const capsArray: string[] = [];

  wordsArray.forEach(word => {
    capsArray.push(word[0].toUpperCase() + word.slice(1));
  });

  return capsArray.join(" ");
}
